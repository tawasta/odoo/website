.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl-3.0-standalone.html
   :alt: License: AGPL-3

=====================================================
Website: Limit Editing Rights by Record Type (Events)
=====================================================

* Allow users to edit their company's event related pages but prevent editing
  other types of pages (blogs, regular pages etc.)

Configuration
=============
* Add the users of your choice to the new "Website: Limit Editing Rights by Record Type (events)"
  group

Usage
=====
* In frontend launch the editor. From non-event pages it will give an access denied error.

Known issues / Roadmap
======================
\-

Credits
=======

Contributors
------------

* Timo Talvitie <timo.talvitie@tawasta.fi>

Maintainer
----------

.. image:: http://tawasta.fi/templates/tawastrap/images/logo.png
   :alt: Oy Tawasta OS Technologies Ltd.
   :target: http://tawasta.fi/

This module is maintained by Oy Tawasta OS Technologies Ltd.
